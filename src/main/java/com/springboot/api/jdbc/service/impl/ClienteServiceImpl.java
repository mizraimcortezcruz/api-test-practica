package com.springboot.api.jdbc.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.api.jdbc.dao.ClienteDao;
import com.springboot.api.jdbc.model.Cliente;
import com.springboot.api.jdbc.service.ClienteService;
@Service
public class ClienteServiceImpl implements ClienteService {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	private ClienteDao clienteDao;
	
	@Override
	public List<Cliente> getAllClientes() {
		return clienteDao.getAllClientes();
	}

	@Override
	public Cliente getCliente(Integer id) {
		return clienteDao.getCliente(id);
	}

	@Override
	public void saveCliente(Cliente cliente) {
		try {
			clienteDao.saveCliente(cliente);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	@Override
	public void deleteCliente(Integer id) {
		try {
			clienteDao.deleteCliente(id);
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
}
