package com.springboot.api.jdbc.service;

import java.util.List;

import com.springboot.api.jdbc.model.Cliente;

public interface ClienteService {
	List<Cliente> getAllClientes();
	Cliente getCliente(Integer id);
	void saveCliente(Cliente cliente);
	void deleteCliente(Integer id);
}
