package com.springboot.api.jdbc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiTestPracticaApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiTestPracticaApplication.class, args);
	}

}
