package com.springboot.api.jdbc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.api.jdbc.model.Cliente;
import com.springboot.api.jdbc.service.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping(value = "/", produces = "application/json")	
	public List<Cliente> getAllClientes(){
		return clienteService.getAllClientes();
	}
	
	@GetMapping(value = "/{id}", produces = "application/json")	
	public Cliente getCliente(@PathVariable ("id") Integer id){
		return clienteService.getCliente(id);
	}
	
	@PostMapping(value = "/", produces = "application/json")	
	public List<Cliente> saveCliente(@RequestBody Cliente cliente){
		clienteService.saveCliente(cliente);
		return clienteService.getAllClientes();
	}	
	
	@DeleteMapping(value = "/{id}", produces = "application/json")
	public List<Cliente> deleteCliente(@PathVariable ("id") Integer id){
		clienteService.deleteCliente(id);
		return clienteService.getAllClientes();
	}	

	
}
